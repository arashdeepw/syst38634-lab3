package password;
//done by arashdeep and jake
public class Validator {
	public static boolean checkPasswordLength(String password) {
		if (password.length()>=8) { //If 8 or more characters
			return true;
		}
		return false;
	}
	public static boolean checkDigits(String password) {
		int count = 0; //Counts digits
		for(int i=0;i<password.length();i++) { //Loop through each character
			if (Character.isDigit(password.charAt(i)))
				count++; //Increase count when digit is found
		}
		if(count>=2) //If 2 or more digits
			return true; //Password is valid
		
		return false;
	}
}
