package password;
//done by arashdeep and jake
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ValidatorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckPasswordLengthPass() {
		assertTrue("Should have returned true",Validator.checkPasswordLength("123qweasdzxc"));
	}
	@Test
	public void testCheckPasswordLengthException() {
		assertTrue("Should have returned false",!Validator.checkPasswordLength("123"));
	}
	@Test
	public void testCheckPasswordLengthBoundaryIn() {
		assertTrue("Should have returned true",Validator.checkPasswordLength("1234qwer"));
	}
	@Test
	public void testCheckPasswordLengthBoundaryOut() {
		assertTrue("Should have returned false",!Validator.checkPasswordLength("123qwe"));
	}

	@Test
	public void testCheckDigitsPass() {
		assertTrue("Should have returned true",Validator.checkDigits("1234"));
	}
	@Test
	public void testCheckDigitsException() {
		assertTrue("Should have returned false",!Validator.checkDigits("asd"));
	}
	@Test
	public void testCheckDigitsBoundaryIn() {
		assertTrue("Should have returned true",Validator.checkDigits("12qwe"));
	}
	@Test
	public void testCheckDigitsBoundaryOut() {
		assertTrue("Should have returned false",!Validator.checkDigits("1qwe"));
	}

}
